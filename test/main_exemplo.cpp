/**
 * @file	main.cpp
 * @brief	Arquivo principal
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @since	28/03/2017
 * @date	30/03/2017
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;

#include "dovector.h"
#include "vetor.h"


/** @brief Funcao principal */
int main() {
	
	// Tipo de dados
	string tipo_dados;
	while (true) {
		cout << "Tipo de dados: ";
		cin >> tipo_dados;
		if (tipo_dados != "int"    && tipo_dados != "float" &&
		    tipo_dados != "double" && tipo_dados != "string") {
			cout << "Tipo de dados invalido. Por favor, tente novamente.\n\n";
		} else {
			break;
		}
	}

	// Tamanho do vetor
	int tamanho;
	cout << "Informe a quantidade de elementos: ";
	cin >> tamanho;

	// Possiveis vetores (serao alocados dinamicamente conforme a necessidade)
	int* vint = NULL;			// Vetor de inteiros
	float* vfloat = NULL;		// Vetor de decimais (float)
	double* vdouble = NULL;		// Vetor de decimais (double)
	string* vstring = NULL;		// Vetor de strings

	// Alocacao, preenchimento e impressao do vetor de acordo com o tipo
	if (tipo_dados == "int") {
		/* Alocar dinamicamente o vetor de inteiros */
		/* Preencher o vetor - funcao genérica - vetor.h*/
		/* Imprimir o vetor  - funcao genérica - vetor.h*/
	} else if (tipo_dados == "float") {
		/* Alocar dinamicamente o vetor de float */
		/* Preencher o vetor - funcao genérica - vetor.h*/
		/* Imprimir o vetor  - funcao genérica - vetor.h*/
	} else if (tipo_dados == "double") {
		/* Alocar dinamicamente o vetor de double */
		/* Preencher o vetor - funcao genérica - vetor.h*/
		/* Imprimir o vetor  - funcao genérica - vetor.h*/
	} else {
		/* Alocar dinamicamente o vetor de string */
		/* Preencher o vetor - funcao genérica - vetor.h*/
		/* Imprimir o vetor  - funcao genérica - vetor.h*/
	}

	// Opcao de operacao
	cout << "\nOperacoes:\n";
	cout << "(1) Maior valor\n";
	cout << "(2) Menor valor\n";
	cout << "(3) Soma dos elementos\n";
	cout << "(4) Media dos elementos\n";
	cout << "(5) Elementos maiores que um valor\n";
	cout << "(6) Elementos menores que um valor\n";
	cout << "(0) Sair" << endl;

	int op = -1;
	while (op != 0) {
		cout << "\nDigite sua opcao: ";
		cin >> op;
		if (op < 0 || op > 6) {
			cout << "Opcao invalida. Por favor, tente novamente.\n";
		}

		// Execucao da operacao propriamente dita de acordo com o tipo do vetor
		if (op != 0) {
			if (tipo_dados == "int") {
				doVector(vint, tamanho, tipo_operacao(op), 0); /* Implementar - funcao genérica - dovetor.h*/
			} else if (tipo_dados == "float") {
				doVector(vfloat, tamanho, tipo_operacao(op), 0); /* Implementar - funcao genérica - dovetor.h*/
			} else if (tipo_dados == "double") {
				doVector(vdouble, tamanho, tipo_operacao(op), 0); /* Implementar - funcao genérica - dovetor.h*/
			} else {
				doVector(vstring, tamanho, tipo_operacao(op), 0); /* Implementar - funcao genérica - dovetor.h*/
			}
		}
	}

	cout << "Programa encerrado." << endl;
	return 0;
}
