/**
 * @file	main.cpp
 * @brief	Codigo fonte principal do programa
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	30/03/2017
 * @date	05/04/2017
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;

#include "dovector.h"
#include "vetor.h"


/** @brief Funcao principal */
int main() {
	
	// Tipo de dados
	string tipo_dados;
	while (true) {
		cout << "Tipo de dados: ";
		cin >> tipo_dados;
		if (tipo_dados != "int"    && tipo_dados != "float" &&
		    tipo_dados != "double" && tipo_dados != "string") {
			cout << "Tipo de dados invalido. Por favor, tente novamente.\n\n";
		} else {
			break;
		}
	}

	// Tamanho do vetor
	int tamanho;
	cout << "Informe a quantidade de elementos: ";
	cin >> tamanho;

	// Possiveis vetores (serao alocados dinamicamente conforme a necessidade)
	int* vint = NULL;			// Vetor de inteiros
	float* vfloat = NULL;		// Vetor de decimais (float)
	double* vdouble = NULL;		// Vetor de decimais (double)
	string* vstring = NULL;		// Vetor de strings

	// Alocacao, preenchimento e impressao do vetor de acordo com o tipo
	if (tipo_dados == "int") {
		vint = new int[tamanho];
		fill_vetor(vint, tamanho);
		print_vetor(vint, tamanho);
	} else if (tipo_dados == "float") {
		vfloat = new float[tamanho];
		fill_vetor(vfloat, tamanho);
		print_vetor(vfloat, tamanho);
	} else if (tipo_dados == "double") {
		vdouble = new double[tamanho];
		fill_vetor(vdouble, tamanho);
		print_vetor(vdouble, tamanho);
	} else {
		vstring = new string[tamanho];
		fill_vetor(vstring, tamanho);
		print_vetor(vstring, tamanho);
	}

	// Opcao de operacao
	cout << "\nOperacoes:\n";
	cout << "(1) Maior valor\n";
	cout << "(2) Menor valor\n";
	cout << "(3) Soma dos elementos\n";
	cout << "(4) Media dos elementos\n";
	cout << "(5) Elementos maiores que um valor\n";
	cout << "(6) Elementos menores que um valor\n";
	cout << "(0) Sair" << endl;

	int operacao = -1;
	while (operacao != 0) {
		cout << "\nDigite sua opcao: ";
		cin >> operacao;
		if (operacao < 0 || operacao > 6) {
			cout << "Opcao invalida. Por favor, tente novamente.\n";
		}

		// Execucao da operacao propriamente dita de acordo com o tipo do vetor
		if (operacao != 0) {
			if (tipo_dados == "int") {
				doVector(vint, tamanho, tipo_operacao(operacao));
			} else if (tipo_dados == "float") {
				doVector(vfloat, tamanho, tipo_operacao(operacao));
			} else if (tipo_dados == "double") {
				doVector(vdouble, tamanho, tipo_operacao(operacao));
			} else {
				doVector(vstring, tamanho, tipo_operacao(operacao));
			}
		}
	}

	cout << "Programa encerrado." << endl;
	return 0;
}
