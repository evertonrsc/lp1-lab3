/**
 * @file	operacao.h
 * @brief	Implementacao das funcoes que realizam as operacoes
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	28/03/2017
 * @date	05/04/2017
 */

#ifndef OPERACAO_H
#define OPERACAO_H

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;


/** 
 * @brief	Funcao generica que retorna o maior elemento numerico de um vetor
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor opcional utilizado como limite ou base quando aplicavel (possui por padrao o valor zero)
 * @return	Maior elemento do vetor
 */
template<typename T>
T opMax(T* vetor, int tamanho, int valor = 0) {
	T max = vetor[0];
	for (int i = 1; i < tamanho; i++) {
		if (vetor[i] > max) {
			max = vetor[i];
		}
	}
	return max;
}


/** 
 * @brief	Funcao especializada que retorna a string de maior tamanho em 
 *			um vetor de strings
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor opcional utilizado como limite ou base quando aplicavel (possui por padrao o valor zero)
 * @return	String de maior tamanho no vetor
 */
template<>
string opMax<string>(string* vetor, int tamanho, int valor) {
	string tmax = vetor[0];
	for (int i = 1; i < tamanho; i++) {
		if (vetor[i].length() > tmax.length()) {
			tmax = vetor[i];
		}
	}
	return tmax;
}


/** 
 * @brief	Funcao generica que retorna o menor elemento numerico de um vetor
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor opcional utilizado como limite ou base quando aplicavel (possui por padrao o valor zero)
 * @return	Menor elemento do vetor
 */
template<typename T>
T opMin(T* vetor, int tamanho, int valor = 0) {
	T min = vetor[0];
	for (int i = 1; i < tamanho; i++) {
		if (vetor[i] < min) {
			min = vetor[i];
		}
	}
	return min;
}


/** 
 * @brief	Funcao especializada que retorna a string de maior tamanho em 
 *			um vetor de strings
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor opcional utilizado como limite ou base quando aplicavel (possui por padrao o valor zero)
 * @return	String de menor tamanho no vetor
 */
template<>
string opMin<string>(string* vetor, int tamanho, int valor) {
	string tmin = vetor[0];
	for (int i = 1; i < tamanho; i++) {
		if (vetor[i].length() < tmin.length()) {
			tmin = vetor[i];
		}
	}
	return tmin;
}


/** 
 * @brief	Funcao generica que retorna a soma dos elementos numericos de um vetor
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor opcional utilizado como limite ou base quando aplicavel (possui por padrao o valor zero)
 * @return	Soma dos elementos do vetor
 */
template<typename T>
T opSum(T* vetor, int tamanho, int valor = 0) {
	T soma = (T)0;
	for (int i = 0; i < tamanho; i++) {
		soma += vetor[i];
	}
	return soma;
}


/** 
 * @brief	Funcao que retorna a concatenacao de todas as strings contidas no vetor
 * @details	Esta funcao sobrecarrega a sua versao generica
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor opcional utilizado como limite ou base quando aplicavel (possui por padrao o valor zero)
 * @return	Concatenacao das strings no vetor
 */
template<>
string opSum<string>(string* vetor, int tamanho, int valor) {
	string concat = "";
	for (int i = 0; i < tamanho; i++) {
		concat += vetor[i];
	}
	return concat;
}


/** 
 * @brief	Funcao generica que retorna a parte inteira do valor medio dos 
 * 			elementos numericos de um vetor
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor opcional utilizado como limite ou base quando aplicavel (possui por padrao o valor zero)
 * @return	Parte inteira do valor medio dos elementos do vetor
 */
template<typename T>
T opAvg(T* vetor, int tamanho, int valor = 0) {
	return (int) opSum(vetor, tamanho) / tamanho;
}


/** 
 * @brief	Funcao especializada que retorna a concatenacao
 *			das strings cujo tamanho e igual a media dos tamanhos de 
 *			todas as strings contidas no vetor. Caso tais strings nao
 *			existam, a função retorna uma string vazia.
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor opcional utilizado como limite ou base quando aplicavel (possui por padrao o valor zero)
 * @return	Concatenacao das strings com tamanho igual a media dos  
 *			tamanhos de todas as strings contidas no vetor
 */
template<>
string opAvg<string>(string* vetor, int tamanho, int valor) {
	string concat = opSum(vetor, tamanho);
	int tamMedio = (int)concat.length() / tamanho;
	string concatMedio = "";
	for (int i = 0; i < tamanho; i++) {
		if ((int)vetor[i].length() == tamMedio) {
			concatMedio += vetor[i];
		}
	}
	return concatMedio;
}


/** 
 * @brief	Funcao generica que retorna o numero de elementos do vetor que
 *			sao maiores que um determinado valor
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor limite
 * @return	Numero de elementos do vetor que sao maiores que o valor em questao
 */
template<typename T>
T opHig(T* vetor, int tamanho, int valor) {
	T qtde = 0;
	for (int i = 0; i < tamanho; i++) {
		if ((int)vetor[i] > valor) {
			qtde++;
		}
	}
	return qtde;
}


/** 
 * @brief	Funcao especializada que retorna a concatenacao das strings 
 *			do vetor cujo tamanho e maior que um determinado valor
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor limite
 * @return	Concatenacao das strings cujo tamanho e maior que o 
 *			valor em questao
 */
template<>
string opHig<string>(string* vetor, int tamanho, int valor) {
	string maiores = "";
	for (int i = 0; i < tamanho; i++) {
		if ((int)vetor[i].length() > valor) {
			maiores += vetor[i];
		}
	}
	return maiores;
}


/** 
 * @brief	Funcao generica que retorna o numero de elementos do vetor que
 *			sao menores que um determinado valor
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor base
 * @return	Numero de elementos do vetor que sao menores que o valor em questao
 */
template<typename T>
T opLow(T* vetor, int tamanho, int valor) {
	T qtde = 0;
	for (int i = 0; i < tamanho; i++) {
		if ((int)vetor[i] < valor) {
			qtde++;
		}
	}
	return qtde;
}


/** 

 * @brief	Funcao especializada que retorna a concatenacao das strings 
 *			do vetor cujo tamanho e meior que um determinado valor
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	valor Valor limite
 * @return	Concatenacao das strings cujo tamanho e menor que o 
 *			valor em questao
 */
template<>
string opLow<string>(string* vetor, int tamanho, int valor) {
	string menores = "";
	for (int i = 0; i < tamanho; i++) {
		if ((int)vetor[i].length() < valor) {
			menores += vetor[i];
		}
	}
	return menores;
}

#endif

