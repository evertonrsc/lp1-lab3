/**
 * @file	vetor.h
 * @brief	Implementacao de funcoes associadas ao gerenciamento de um vetor
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	28/03/2017
 * @date	05/04/2017
 */

#ifndef VETOR_H
#define VETOR_H

#include <cstdlib>
using std::rand;

#include <ctime>
using std::time;

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;


/** 
 * @brief	Funcao generica que imprime os elementos de um vetor 
 *			de elementos numericos
 * @param 	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 */
template<typename T>
void print_vetor(T* vetor, int tamanho) {
	cout << "Vetor: [";
	for (int i = 0; i < tamanho; i++) {
		cout << vetor[i];
		if (i != tamanho-1) {
			cout << ", ";
		}
	}
	cout << "]" << endl;
}


/** 
 * @brief	Funcao especializada que imprime os elementos de um 
 *			vetor de strings
 * @param 	vetor Vetor de strings em questao
 * @param	tamanho Tamanho do vetor
 */
template<>
void print_vetor(string* vetor, int tamanho) {
	cout << "Vetor: [";
	for (int i = 0; i < tamanho; i++) {
		cout << "\"" << vetor[i] << "\"";
		if (i != tamanho-1) {
			cout << ", ";
		}
	}
	cout << "]" << endl;
}


/**
 * @brief Funcao generica que gera elementos numericos para serem armazenados 
 *		  em um vetor de forma randomica
 * @param vetor Vetor em questao
 * @param tamanho Tamanho do vetor
 */
template<typename T>
void fill_vetor(T* vetor, int tamanho) {
	srand(time(NULL));								// Semente para geracao randomica
	for (int i = 0; i < tamanho; i++) {
		vetor[i] = (T)(rand() % tamanho) + (T)1.0;	// Numero randomico
	}
}


/**
 * @brief Funcao especializada para preencher um vetor com strings fornecidas
 *		  pelo usuario
 * @param vetor Vetor em questao
 * @param tamanho Tamanho do vetor
 */
template<>
void fill_vetor<string>(string* vetor, int tamanho) {
	for (int i = 0; i < tamanho; i++) {
		cout << "Digite uma string a ser armazenada no vetor: ";
		cin >> vetor[i];
	}
}

#endif
