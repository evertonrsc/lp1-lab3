/**
 * @file	dovector.h
 * @brief	Implementacao da funcao doVector, que realiza a execucao da 
 *			operacao solicitada pelo usuario sobre o vetor
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	28/03/2017
 * @date	05/04/2017
 */

#ifndef DOVECTOR_H
#define DOVECTOR_H

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include "operacao.h"
#include "typeop.h"


/**
 * @brief 	Funcao generica que realiza a execucao da operacao solicitada pelo 
 * 			usuario sobre o vetor de elementos numericos
 * @param	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	operacao Operacao a ser realizada, definida como um tipo enumeravel
 * @param	valor Valor opcional utilizado como limite ou base quando aplicavel (possui por padrao o valor zero)
 */
template<typename T>
void doVector(T* vetor, int tamanho, tipo_operacao operacao, int valor = 0) {
	// Ponteiro de funcao que aponta para a funcao a ser executada
	T (*pontfunc)(T*,int,int);

	switch (operacao) {
		case opmax:
			cout << "Maior elemento no vetor: ";
			pontfunc = opMax;
			break;
		case opmin:
			cout << "Menor elemento no vetor: ";
			pontfunc = opMin;
			break;
		case opsum:
			cout << "Soma dos elementos do vetor: ";
			pontfunc = opSum;
			break;
		case opavg:
			cout << "Media dos elementos do vetor: ";
			pontfunc = opAvg;
			break;
		case ophig:
			cout << "Informe o valor limite: ";
			cin >> valor;
			cout << "Quantidade de elementos maiores que " << valor << ": ";
			pontfunc = opHig;
			break;
		case oplow:
			cout << "Informe o valor base: ";
			cin >> valor;
			cout << "Quantidade de elementos menores que " << valor << ": ";
			pontfunc = opLow;
			break;
	}

	// Execucao da funcao selecionada
	cout << pontfunc(vetor, tamanho, valor) << endl;
}


/**
 * @brief 	Funcao especializada que realiza a execucao da operacao solicitada pelo 
 * 			usuario sobre o vetor de strings
 * @param	vetor Vetor em questao
 * @param	tamanho Tamanho do vetor
 * @param	operacao Operacao a ser realizada, definida como um tipo enumeravel
 * @param	valor Valor opcional utilizado como limite ou base quando aplicavel (possui por padrao o valor zero)
 */
template<>
void doVector<string>(string* vetor, int tamanho, tipo_operacao operacao, int valor) {
	string (*pontfunc)(string*,int,int);
	switch (operacao) {
		case opmax:
			cout << "Maior string no vetor: ";
			pontfunc = opMax;
			break;
		case opmin:
			cout << "Menor string no vetor: ";
			pontfunc = opMin;
			break;
		case opsum:
			cout << "Concatenacao das strings no vetor: ";
			pontfunc = opSum;
			break;
		case opavg:
			cout << "Concatenacao das strings com tamanho medio: ";
			pontfunc = opAvg;
			break;
		case ophig:
			cout << "Informe o valor limite: ";
			cin >> valor;
			cout << "Concatenacao das strings com tamanho maior que " << valor << ": ";
			pontfunc = opHig;
			break;
		case oplow:
			cout << "Informe o valor base: ";
			cin >> valor;
			cout << "Concatenacao das strings com tamanho menor que " << valor << ": ";
			pontfunc = opLow;
			break;
	}

	// Execucao da funcao selecionada
	cout << pontfunc(vetor, tamanho, valor) << endl;
}

#endif
