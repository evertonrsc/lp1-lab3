/**
 * @file	typeop.h
 * @brief	Definicao de um tipo enumeravel contendo as operacoes que podem 
 *			ser realizadas
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	28/03/2017
 * @date	05/04/2017
 */

#ifndef TYPEOP_H
#define TYPEOP_H

/** 
 * @brief 	Tipo enumeravel contendo as operacoes que podem ser realizadas
 * @details A enumeracao comeca com o valor 1 e os valores subsequentes 
 *			sao automaticamente incrementados em uma unidade
 */
enum tipo_operacao {
	opmax = 1,	/**< Retorna o maior valor presente no vetor */
	opmin,		/**< Retorna o menor valor presente no vetor */
	opsum,  	/**< Retorna a soma de todos os valores presentes no vetor */
	opavg,		/**< Retorna o valor medio (parte inteira) dos valores presentes no vetor */
	ophig,		/**< Retorna a quantidade de elementos maiores que um determinado valor */
	oplow		/**< Retorna a quantidade de elementos menores que um determinado valor */
};

#endif
